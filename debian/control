Source: hg-git
Section: vcs
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Tristan Seligmann <mithrandi@debian.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 git,
 mercurial (>= 4.8~),
 openssh-client,
 python (>= 2.6.6-3~),
 python-dulwich,
 unzip,
Standards-Version: 4.4.1
Homepage: https://hg-git.github.io/
Vcs-Git: https://salsa.debian.org/python-team/applications/hg-git.git
Vcs-Browser: https://salsa.debian.org/python-team/applications/hg-git

Package: mercurial-git
Architecture: all
Depends:
 mercurial (>= 4.8~),
 python-dulwich (>= 0.9.7),
 ${misc:Depends},
 ${python:Depends},
Description: Git plugin for Mercurial
 The Hg-Git plugin for Mercurial adds the ability to push and pull to/from
 a Git server repository. This means you can collaborate on Git based
 projects from Mercurial, or use a Git server as a collaboration point
 for a team with developers using both Git and Mercurial.
 .
 The plugin can convert commits/changesets losslessly from one system to
 another, so you can push via a Mercurial repository and another
 Mercurial client can pull it and their changeset node ids will be
 identical - Mercurial data does not get lost in translation.
